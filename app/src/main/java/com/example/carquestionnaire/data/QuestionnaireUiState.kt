package com.example.carquestionnaire.data

data class QuestionnaireUiState (
    val engineDisplacementAnswer: String = "",
    val engineInductionAnswer: String = "",
    val username: String = "",
    val firstScreenAnswer: String = "",
    val numRalliesWon: Int = 0,
    val lamboFasterThanLancia: Boolean = false,
    val finalRating: Int = 0,
)