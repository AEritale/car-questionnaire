package com.example.carquestionnaire.data

object DataSource {
    val engineDisplacements = listOf(
        "2.0L V6",
        "1.8L I4",
        "2.2L I4",
        "2.0L Boxer 4"
    )

    val engineInductions = listOf(
        "Naturally aspirated",
        "Turbocharged",
        "Supercharged",
        "Twin-charged"
    )
}