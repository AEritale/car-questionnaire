@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.carquestionnaire

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.carquestionnaire.data.DataSource
import com.example.carquestionnaire.ui.QuestionnaireViewModel
import com.example.carquestionnaire.ui.screens.FinalScreen
import com.example.carquestionnaire.ui.screens.FirstChoiceScreen
import com.example.carquestionnaire.ui.screens.ListChoiceScreen
import com.example.carquestionnaire.ui.screens.StarEvaluationScreen
import com.example.carquestionnaire.ui.screens.StartScreen
import com.example.carquestionnaire.ui.screens.YesNoScreen

enum class CarQuestionnaire(val title: String) {
    WELCOME("WELCOME"),
    YESNO("YESNO"),
    CHOICE1("CHOICE1"),
    CHOICE2("CHOICE2"),
    CHOICE3("CHOICE3"),
    STAR1("STAR1"),
    STAR2("STAR2"),
    FINAL("FINAL"),
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppBar(
    canNavigateBack: Boolean,
    currentScreen: CarQuestionnaire,
    navigateUp: () -> Unit,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = {
            Text(
                text = stringResource(id = R.string.appBar_title),
                textAlign = TextAlign.Center,
            )
        },
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
        modifier = modifier,
        navigationIcon = {
            if (canNavigateBack) {
                IconButton(onClick = navigateUp) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = stringResource(R.string.back_button)
                    )
                }
            }
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CarQuestionnaireApp(
    viewModel: QuestionnaireViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    val backStackEntry by navController.currentBackStackEntryAsState()

    val currentScreen = CarQuestionnaire.valueOf(
        backStackEntry?.destination?.route ?: CarQuestionnaire.WELCOME.name
    )

    Scaffold(
        topBar = {
            AppBar(
                currentScreen = currentScreen,
                canNavigateBack = navController.previousBackStackEntry != null,
                navigateUp = { navController.navigateUp() }
            )
        }
    ) { innerPadding ->
        val uiState by viewModel.uiState.collectAsState()

        NavHost(
            navController = navController,
            startDestination = CarQuestionnaire.WELCOME.name,
            modifier = Modifier.padding(innerPadding),
        ) {
            composable(route = CarQuestionnaire.WELCOME.name) {
                StartScreen(
                    onNextButtonClicked = {
                        viewModel.setUsername(it)
                        navController.navigate(CarQuestionnaire.CHOICE1.name)
                    },
                )
            }
            composable(route = CarQuestionnaire.CHOICE1.name) {
                FirstChoiceScreen(
                    onNextButtonClicked = {
                        viewModel.setFirstScreenAnswer(it)
                        navController.navigate(CarQuestionnaire.CHOICE2.name)
                    }
                )
            }
            composable(route = CarQuestionnaire.CHOICE2.name) {
                ListChoiceScreen(
                    optionsList = DataSource.engineDisplacements,
                    image = painterResource(id = R.drawable.deltas4_engine),
                    prompt = stringResource(id = R.string.choice2Prompt),
                    onNextButtonClicked = {
                        viewModel.setEngineDisplacementAnswer(it)
                        navController.navigate(CarQuestionnaire.STAR1.name)
                    }
                )
            }
            composable(route = CarQuestionnaire.STAR1.name) {
                StarEvaluationScreen(
                    prompt = stringResource(id = R.string.star1Prompt),
                    image = painterResource(R.drawable.deltas4rallying),
                    onNextButtonClicked = {
                        viewModel.setNumRalliesWon(it)
                        navController.navigate(CarQuestionnaire.CHOICE3.name)
                    }
                )
            }
            composable(route = CarQuestionnaire.CHOICE3.name) {
                ListChoiceScreen(
                    optionsList = DataSource.engineInductions,
                    image = painterResource(id = R.drawable.deltas4enginebay),
                    prompt = stringResource(id = R.string.choice3Prompt),
                    onNextButtonClicked = {
                        viewModel.setEngineInductionAnswer(it)
                        navController.navigate(CarQuestionnaire.YESNO.name)
                    }
                )
            }
            composable(route = CarQuestionnaire.YESNO.name) {
                YesNoScreen(
                    prompt = stringResource(id = R.string.yesNoPrompt),
                    image1 = painterResource(R.drawable.huracan_sto),
                    image1Desc = "Lamborghini Huracan STO",
                    image2 = painterResource(R.drawable.lancia_delta_s4_drive),
                    image2Desc = "Lancia Delta S4",
                    onNextButtonClicked = {
                        viewModel.setLamboFasterThanLancia(it)
                        navController.navigate(CarQuestionnaire.STAR2.name)
                    }
                )
            }
            composable(route = CarQuestionnaire.STAR2.name) {
                StarEvaluationScreen(
                    prompt = stringResource(id = R.string.star2Prompt),
                    image = painterResource(R.drawable.deltas4_finish),
                    onNextButtonClicked = {
                        viewModel.setFinalRating(it)
                        navController.navigate(CarQuestionnaire.FINAL.name)
                    }
                )
            }
            composable(route = CarQuestionnaire.FINAL.name) {
                val userName = uiState.username
                FinalScreen(
                    title = "Thank you $userName for taking the quiz. \nYou can check the answers down below",
                    firstAnswer = uiState.firstScreenAnswer,
                    displacementAnswer = uiState.engineDisplacementAnswer,
                    inductionAnswer = uiState.engineInductionAnswer,
                    numRalliesAnswer = uiState.numRalliesWon,
                    lamboIsFasterAnswer = uiState.lamboFasterThanLancia,
                    finalReview = uiState.finalRating,
                    restartQuiz = { restartQuiz(viewModel, navController) }
                )
            }
        }
    }
}


private fun restartQuiz(
    viewModel: QuestionnaireViewModel,
    navController: NavHostController
){
    viewModel.resetAnswers()
    navController.popBackStack(CarQuestionnaire.WELCOME.name, inclusive = false)
}