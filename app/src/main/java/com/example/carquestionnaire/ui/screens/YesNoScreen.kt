package com.example.carquestionnaire.ui.screens

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.carquestionnaire.R

@Composable
fun YesNoScreen(
    onNextButtonClicked: ( Boolean ) -> Unit = {},
    image1: Painter,
    image1Desc: String,
    image2: Painter,
    image2Desc: String,
    prompt: String,
    modifier: Modifier = Modifier
) {
    var answer: Boolean by rememberSaveable {
        mutableStateOf( true )
    }

    var hasAnswered: Boolean by rememberSaveable {
      mutableStateOf( false )
    }

    Column (
        modifier = modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {


        Spacer(modifier = Modifier.height(20.dp))

        Text(
            modifier = Modifier.padding(20.dp),
            textAlign = TextAlign.Center,
            text = prompt
        )

        Spacer(modifier = Modifier.height(20.dp))

        Row(
            modifier = modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Column(
                modifier = modifier.width(200.dp),
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(text = image1Desc)

                Image(
                    painter = image1,
                    contentDescription = null,
                    modifier = Modifier
                        .width(200.dp)
                        .height(130.dp)
                )
                Button(
                    colors = ButtonDefaults.buttonColors(
                        if(hasAnswered && answer) MaterialTheme.colorScheme.primary
                        else Color.Transparent
                    ),
                    border = BorderStroke(2.dp, MaterialTheme.colorScheme.primary),
                    onClick = {
                        answer = true
                        hasAnswered = true
                              },
                ){
                    Text(
                        text = "Yes",
                        color = if(hasAnswered && answer) Color.White else MaterialTheme.colorScheme.primary
                    )
                }
            }
            Column(
                modifier = modifier.width(200.dp),
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(text = image2Desc)
                Image(
                    painter = image2,
                    contentDescription = null,
                    modifier = Modifier
                        .width(200.dp)
                        .height(130.dp)
                )

                Button(
                    colors = ButtonDefaults.buttonColors(
                        containerColor =
                        if(hasAnswered && !answer) MaterialTheme.colorScheme.primary
                        else Color.Transparent
                    ),
                    border = BorderStroke(2.dp, MaterialTheme.colorScheme.primary),
                    onClick = {
                        answer = false
                        hasAnswered = true
                    },

                ){
                    Text(
                        text = "No",
                        color = if(hasAnswered && !answer) Color.White else MaterialTheme.colorScheme.primary
                    )
                }
            }
        }

        Spacer(modifier = Modifier.height(20.dp))

        Button(
            onClick = { onNextButtonClicked(answer) },
            enabled = hasAnswered,
            modifier = modifier.widthIn(min = 250.dp)
        ) {
            Text(text = stringResource(id = R.string.nextQuestion))
        }
    }
}

@Preview
@Composable
fun YesNoScreenPreview(){
    YesNoScreen(
        onNextButtonClicked = { /*TODO*/ },
        image1 = painterResource(R.drawable.huracan_sto),
        image1Desc = "Lamborghini Huracan STO",
        image2 = painterResource(R.drawable.lancia_delta_s4_drive),
        image2Desc = "Lancia Delta S4",
        prompt = stringResource(id = R.string.yesNoPrompt))
}