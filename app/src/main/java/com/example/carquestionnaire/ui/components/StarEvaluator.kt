package com.example.carquestionnaire.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp


@Composable
fun StarEvaluator(
    modifier: Modifier = Modifier,
    onClick: (Int) -> Unit
){
    var numStars by rememberSaveable { mutableStateOf(0) }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(24.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ){
        Icon(
            imageVector = Icons.Default.Star,
            contentDescription = null,
            tint = if(numStars >= 1) Color(236, 177, 0, 255) else Color.LightGray,
            modifier = Modifier.clickable {
                onClick(1)
                numStars = 1
            }
        )
        Icon(
            imageVector = Icons.Default.Star,
            contentDescription = null,
            tint = if(numStars >= 2) Color(236, 177, 0, 255) else Color.LightGray,
            modifier = Modifier.clickable {
                onClick(2)
                numStars = 2
            }
        )
        Icon(
            imageVector = Icons.Default.Star,
            contentDescription = null,
            tint = if(numStars >= 3) Color(236, 177, 0, 255) else Color.LightGray,
            modifier = Modifier.clickable {
                onClick(3)
                numStars = 3
            }
        )
        Icon(
            imageVector = Icons.Default.Star,
            contentDescription = null,
            tint = if(numStars >= 4) Color(236, 177, 0, 255) else Color.LightGray,
            modifier = Modifier.clickable {
                onClick(4)
                numStars = 4
            }
        )
        Icon(
            imageVector = Icons.Default.Star,
            contentDescription = null,
            tint = if(numStars >= 5) Color(236, 177, 0, 255) else Color.LightGray,
            modifier = Modifier.clickable {
                onClick(5)
                numStars = 5
            }
        )
    }
}
