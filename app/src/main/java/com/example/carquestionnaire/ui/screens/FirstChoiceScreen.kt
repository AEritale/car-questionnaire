package com.example.carquestionnaire.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.Button
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.carquestionnaire.R

@Composable
fun FirstChoiceScreen(
    onNextButtonClicked: (String) -> Unit = {},
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
    ){

        var selectedPicture by rememberSaveable { mutableStateOf("")}

        Spacer(modifier = Modifier.height(20.dp))

        Text(
            modifier = Modifier.padding(20.dp),
            textAlign = TextAlign.Center,
            text = stringResource(id = R.string.choice1Prompt)
        )

        Spacer(modifier = Modifier.height(20.dp))

        Row(
            modifier  = modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ){
            Column (
                modifier = modifier.width(200.dp),
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Image(
                    painter = painterResource(R.drawable._37),
                    contentDescription = null,
                    modifier = Modifier
                        .width(200.dp)
                        .height(130.dp)
                )
                RadioButton(
                    selected = selectedPicture == "037",
                    onClick = {
                        selectedPicture = "037"
                    }
                )
            }
            Column (
                modifier = modifier.width(200.dp),
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Image(
                    painter = painterResource(R.drawable.deltas4),
                    contentDescription = null,
                    modifier = Modifier
                        .width(200.dp)
                        .height(130.dp)
                )
                RadioButton(
                    selected = selectedPicture == "S4",
                    onClick = {
                        selectedPicture = "S4"
                    }
                )
            }
        }

        Spacer(modifier = Modifier.height(20.dp))

        Button(
            onClick = { onNextButtonClicked( selectedPicture ) },
            enabled = selectedPicture != "",
            modifier = modifier.widthIn(min = 250.dp)
        ){
            Text(text = stringResource(id = R.string.nextQuestion))
        }
    }
}

@Preview
@Composable
fun FirstChoiceScreenPreview(){
    FirstChoiceScreen(onNextButtonClicked = { /*TODO*/ })
}