package com.example.carquestionnaire.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.carquestionnaire.R
import com.example.carquestionnaire.ui.components.StarEvaluator

@Composable
fun StarEvaluationScreen(
    onNextButtonClicked: (Int) -> Unit = {},
    prompt: String,
    image: Painter,
    modifier: Modifier = Modifier
){
    var stars by rememberSaveable { mutableStateOf(0) }

    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceBetween
    ){
        Text(
            modifier = Modifier.padding(20.dp),
            textAlign = TextAlign.Center,
            text = prompt
        )
        
        StarEvaluator(
            onClick = { stars = it }
        )

        Image(
            painter = image,
            contentDescription = null,
            modifier = Modifier.fillMaxWidth(),
        )
        Button(
            onClick = { onNextButtonClicked( stars ) },
            enabled = stars != 0,
            modifier = modifier.widthIn(min = 250.dp)
        ){
            Text(text = stringResource(id = R.string.nextQuestion))
        }
    }
}

@Preview
@Composable
fun previewStarEvaluationScreen(){
    StarEvaluationScreen(
        prompt = stringResource(id = R.string.star1Prompt),
        image = painterResource(R.drawable.deltas4rallying)
    )
}