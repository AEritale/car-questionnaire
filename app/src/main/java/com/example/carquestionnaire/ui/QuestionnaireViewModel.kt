package com.example.carquestionnaire.ui

import androidx.lifecycle.ViewModel
import com.example.carquestionnaire.data.QuestionnaireUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class QuestionnaireViewModel : ViewModel() {

    private val _uiState = MutableStateFlow(QuestionnaireUiState())
    val uiState: StateFlow<QuestionnaireUiState> = _uiState.asStateFlow()

    fun setEngineDisplacementAnswer(engineDisplacementAnswer: String){
        _uiState.update { currentState ->
            currentState.copy(
                engineDisplacementAnswer = engineDisplacementAnswer,
            )
        }
    }

    fun setEngineInductionAnswer(engineInductionAnswer: String){
        _uiState.update { currentState ->
            currentState.copy(
                engineInductionAnswer = engineInductionAnswer,
            )
        }
    }

    fun setUsername(username: String){
        _uiState.update { currentState ->
            currentState.copy(
                username = username,
            )
        }
    }

    fun setFirstScreenAnswer(carChoice: String){
        _uiState.update { currentState ->
            currentState.copy(
                firstScreenAnswer = carChoice
            )
        }
    }

    fun setNumRalliesWon(numRallies: Int){
        _uiState.update { currentState ->
            currentState.copy(
                numRalliesWon = numRallies
            )
        }
    }

    fun setLamboFasterThanLancia(lamboIsFaster: Boolean){
        _uiState.update { currentState ->
            currentState.copy(
                lamboFasterThanLancia = lamboIsFaster
            )
        }
    }

    fun setFinalRating(valuation: Int){
        _uiState.update { currentState ->
            currentState.copy(
                finalRating = valuation
            )
        }
    }

    fun resetAnswers(){
        _uiState.value = QuestionnaireUiState()
    }
}

