package com.example.carquestionnaire.ui.screens

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.carquestionnaire.R


@Composable
fun FinalScreen(
    title: String,
    firstAnswer: String,
    displacementAnswer: String,
    inductionAnswer: String,
    numRalliesAnswer: Int,
    lamboIsFasterAnswer: Boolean,
    finalReview: Int,
    restartQuiz: () -> Unit,
    modifier: Modifier = Modifier
) {

    Column(
        modifier = Modifier
            .fillMaxWidth()

            .padding(12.dp)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        Text(
            modifier = Modifier.padding(20.dp),
            textAlign = TextAlign.Center,
            text = title,
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
            )
        )

        Divider(
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(2.dp)
                .padding(horizontal = 12.dp)
        )

        Text(
            text = "You selected this car as the Lancia Delta S4",
            modifier = Modifier.padding(8.dp),
            textAlign = TextAlign.Center,
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
            )
        )
        if (firstAnswer == "037") {
            Image(
                painter =
                painterResource(id = R.drawable._37),
                contentDescription = null,
                modifier = Modifier.width(200.dp)
            )
            Text(
                text = "Wrong.",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color.Red
                )
            )
            Text(
                text = "This is the Lancia Delta S4.",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                )
            )
            Image(
                painter =
                painterResource(id = R.drawable.deltas4),
                contentDescription = null,
                modifier = Modifier.width(200.dp)
            )
        } else {
            Image(
                painter =
                painterResource(id = R.drawable.deltas4),
                contentDescription = null,
                modifier = Modifier.width(200.dp)
            )
            Text(
                text = "Correct!",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color(0, 181, 48, 255)
                )
            )
        }

        Divider(
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(2.dp)
                .padding(horizontal = 12.dp)
        )

        Text(
            text = "You selected $displacementAnswer as the engine of the Lancia Delta S4",
            modifier = Modifier.padding(8.dp),
            textAlign = TextAlign.Center,
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
            )
        )

        if (displacementAnswer == "1.8L I4") {
            Text(
                text = "Correct!",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color(0, 181, 48, 255)
                )
            )
        } else {
            Text(
                text = "Wrong.\nThe engine found in the Delta S4 is a 1.8L I4",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color.Red
                )
            )

        }

        Divider(
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(2.dp)
                .padding(horizontal = 12.dp)
        )

        Text(
            text = "You said that the Delta S4 won $numRalliesAnswer rallies during its career (1985-1986)",
            modifier = Modifier.padding(8.dp),
            textAlign = TextAlign.Center,
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
            )
        )

        if (numRalliesAnswer == 5) {
            Text(
                text = "Correct!",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color(0, 181, 48, 255)
                )
            )
        } else {
            Text(
                text = "Wrong.\nThe Delta S4 has won 5 rallies out of 12 entries during its career",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color.Red
                )
            )
        }

        Divider(
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(2.dp)
                .padding(horizontal = 12.dp)
        )

        Text(
            text = "You said that the Delta S4 engine has a $inductionAnswer induction",
            modifier = Modifier.padding(8.dp),
            textAlign = TextAlign.Center,
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.SemiBold,
            )
        )

        if (inductionAnswer == "Twin-charged") {
            Text(
                text = "Correct!",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color(0, 181, 48, 255)
                )
            )
        } else {
            Text(
                text = "Wrong.\nThe Delta S4 engine is Twin-charged. This means that it is equipped with both a supercharger and a turbocharger",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color.Red
                )
            )
        }

        Divider(
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(2.dp)
                .padding(horizontal = 12.dp)
        )

        if (!lamboIsFasterAnswer) {

            Text(
                text = "You said that the Delta S4 is faster than the Lamborghini STO.",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.SemiBold,
                )
            )
            Text(
                text = "Correct!",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color(0, 181, 48, 255)
                )
            )
        } else {
            Text(
                text = "You said that the Lamborghini STO is faster than the Delta S4.",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.SemiBold,
                )
            )
            Text(
                text = "Wrong.",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 16.sp,
                    color = Color.Red
                )
            )
        }
        Text(
            text = "The official 0-100km/h of the Lamborghini STO is 3.0 seconds.\nMeanwhile the Delta S4 can reach 100km/h in 2.5 seconds ON GRAVEL!",
            modifier = Modifier.padding(8.dp),
            textAlign = TextAlign.Center,
            style = TextStyle(
                fontSize = 16.sp,
            )
        )

        Divider(
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier
                .fillMaxWidth()
                .height(2.dp)
                .padding(horizontal = 12.dp)
        )

        if (finalReview >= 3) {
            Text(
                text = "Thank you very much for taking the quiz, and for the $finalReview star review :)",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.SemiBold,
                )
            )
        } else {
            Text(
                text = "Thank you very much for taking the quiz.\nI hope that you enjoyed the quiz regardless of the review :)",
                modifier = Modifier.padding(8.dp),
                textAlign = TextAlign.Center,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.SemiBold,
                )
            )
        }
        Button(
            colors = ButtonDefaults.buttonColors(MaterialTheme.colorScheme.primary),
            border = BorderStroke(2.dp, MaterialTheme.colorScheme.primary),
            onClick = restartQuiz,
            modifier = modifier.widthIn(min = 250.dp)
        ){
            Text(
                text = "Restart quiz",
                color = Color.White
            )
        }
    }
}