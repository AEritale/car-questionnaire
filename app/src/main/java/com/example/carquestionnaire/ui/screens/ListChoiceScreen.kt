package com.example.carquestionnaire.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.selection.selectable
import androidx.compose.material3.Button
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.carquestionnaire.R

@Composable
fun ListChoiceScreen(
    onNextButtonClicked: (String) -> Unit = {},
    optionsList: List<String>,
    image: Painter,
    prompt: String,
    modifier: Modifier = Modifier
){

    var selection by rememberSaveable { mutableStateOf("")}
    Column(
        modifier = modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,

        ){
        Spacer(modifier = Modifier.height(20.dp))

        Text(
            modifier = Modifier.padding(20.dp),
            textAlign = TextAlign.Center,
            text = prompt
        )

        Spacer(modifier = Modifier.height(12.dp))

        Column(modifier = Modifier.padding(16.dp)){
            optionsList.forEach{option ->
                Row(
                    modifier = Modifier.selectable(
                        selected = selection == option,
                        onClick = {
                            selection = option
                        }
                    ),
                    verticalAlignment = Alignment.CenterVertically
                ){
                    RadioButton(
                        selected = selection == option,
                        onClick = {
                            selection = option
                        }
                    )
                    Text(option)
                }
            }
        }
        Image(
            painter = image,
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .padding(12.dp),
        )
        Button(
            onClick = { onNextButtonClicked( selection ) },
            enabled = selection != "",
            modifier = modifier.widthIn(min = 250.dp)
        ){
            Text(text = stringResource(id = R.string.nextQuestion))
        }
    }
}

@Preview
@Composable
fun previewListChoiceScreen(){
    ListChoiceScreen(
        optionsList = listOf(
        "2.0L V6",
        "1.8L I4",
        "2.2L I4",
        "2.0L Boxer 4"
    ),
    image = painterResource(id = R.drawable.deltas4_engine),
        prompt = stringResource(id = R.string.choice2Prompt)
    )
}